/**
 * The script is encapsulated in an self-executing anonymous function,
 * to avoid conflicts with other libraries
 */
// (function($) {


	/**
	 * Declare 'use strict' to the more restrictive code and a bit safer,
	 * sparing future problems
	 */
	"use strict";

   $(window).scroll(function(){
   var scroll = $(this).scrollTop();
   if(scroll >= 150){
      $('.navbar').removeClass('navbar-grey');
   }
   else{
    $('.navbar').addClass('navbar-grey');
   }
})

   function fechaNavbar (){
    $('.navbar-collapse').removeClass("in");
    $('html').removeClass('open')
   }


   $('.navbar-toggle').on('click',function(){
    console.log($('.navbar-collapse').hasClass("in"))
      if(!$('.navbar-collapse').hasClass("in")){
        $('html').addClass("open");
      }else {
        $('html').removeClass("open");
      }
   });

   $("html, body").on("click", function(e){
    if(e.target == document.documentElement){
      fechaNavbar();
    }
   })




$(function(){
	if ($(window).width() >= 1200) {
		$('#myCarousel1').carousel({interval:false});
	}
})

$(document).ready(function(){

  $('.ancora button').click(function(e){

      e.stopPropagation();
      e.cancelBubble = true;

  });

  // Add smooth scrolling to all links
  $(".ancora").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "" && typeof this.hash != 'undefined' ) {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop:  ($(hash).offset().top-120)
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
        fechaNavbar();
      });
    } // End if
  });
});


function enviarContato(caminho,dados,msgconfig){
      $.ajax({
        url:caminho,
        type:"POST",
        data:dados,
        dataType:'json',
        beforeSend: function(){
          msgconfig.status.addClass('hidden');
        },
        success: function(retorno){
          if(retorno.status){
            msgconfig.enviado.removeClass("hidden");
          }else{
            msgconfig.status.addClass('hidden');
            msgconfig.problema.removeClass('hidden');
          }
        },
        error: function(){
          msgconfig.problema.removeClass('hidden');
        }
      })
    }

    function validar_campo($campo){
      if($campo.val() == ""){
        $campo.addClass("form-error");
        $campo.parent().find(".help-block").addClass("valid-error");
        return false;
      }
      $campo.removeClass("form-error");
      $campo.parent().find(".help-block").removeClass("valid-error");
      return true;
    }

    /* form validation  formulario visita-banner*/
    $('#contact-form1').submit(function(evento){
      evento.preventDefault();

      var situacao_do_form = true;

      var nome = $('#nome');
      var email = $('#email');
      var telefone = $('#telefone');

      situacao_do_form = validar_campo(nome);
      situacao_do_form = validar_campo(email);
      situacao_do_form = validar_campo(telefone);

      var msconfig = {
        status: $('.form-status'),
        enviado: $('.form-enviado'),
        problema: $('.form-problema')
      }


      if(situacao_do_form){
        enviarContato('mail.php',$(this).serialize(),msconfig);
      }

    })

/* form validation  formulario visita-banner*/
    $('#contact-form2').submit(function(evento){
      evento.preventDefault();

      var situacao_do_form = true;

      var nomeModal = $('#nomeModal');
      var emailModal = $('#emailModal');
      var telefoneModal = $('#telefoneModal');
      var enderecoModal = $('#enderecoModal');
      var cidadeModal = $('#cidadeModal');
      var estadoModal = $('#estadoModal');
      var metrosModal = $('#metrosModal');
      var emailModal = $('#emailModal');
      var ambienteModal = $('#ambienteModal');
      var investimentoModal = $('#investimentoModal');

      situacao_do_form = validar_campo(nome);
      situacao_do_form = validar_campo(email);
      situacao_do_form = validar_campo(telefone);

      var msconfig = {
        status: $('.form-status'),
        enviado: $('.form-enviado'),
        problema: $('.form-problema')
      }


      if(situacao_do_form){
        enviarContato('mail.php',$(this).serialize(),msconfig);
      }

    })





  